﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tut02_Ex06
{
    class Program
    {
        static void Main(string[] args)
        {
            var counter = 5;
            var i = 0;

            while (i < counter)
            {
                var a = i + 1;
                Console.WriteLine($"This is line number {a}");

                i++;
            }

        }
    }
}
